const {UserModel} = require('../../models/user');

module.exports.showDashboardPageController = (req, res) =>{
    console.log(req.session);
    if(!req.session.userID){
        return res.redirect('/login');
    }

        UserModel.findById(req.session.userID,(err,user)=>{
            if(user){
                res.render('dashboard',{user: user.name})
            }

            if(!user){
                res.red('/login');
            }
        })


};

module.exports.showLoginPageController = (req, res) =>{
    res.render('login',{message: req.query.message})
};

module.exports.showRegisterPageController = (req, res) =>{
    res.render('register',{message: req.query.message})
};