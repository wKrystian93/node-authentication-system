const {UserModel} = require('../../models/user');

module.exports.registerActionController = async (req, res) => {
    //get data from client and save it to database
    const userData = req.body;

    const user = await UserModel.findOne({email: userData.email}).exec();

    if (user) {
        res.redirect('/login?message='+encodeURIComponent('User already exist. You can login to your account.'));
        return;
    }

    const newUser = new UserModel(userData);
    newUser.save(null, (err,user)=>{
        req.session.userID = user._id;
        res.redirect('/dashboard');
    });

};

module.exports.loginActionController = async (req, res) => {
    const userData = req.body;

    const user = await UserModel.findOne({email: userData.email, password: userData.password}).exec();
    console.log(user);

    if (user) {
        req.session.userID = user._id;
        res.redirect('/dashboard');
    }else{
        res.redirect('/register?message='+encodeURIComponent('User do not exist. Create new account.'));
    }
};







