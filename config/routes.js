const {showRegisterPageController,showDashboardPageController,showLoginPageController} = require("../controllers/user/user-page-controller");
const {registerActionController, loginActionController} = require('../controllers/user/user-action-controller');

const {showHomePageController} = require("../controllers/home");

module.exports.registerRoutes = (app) =>{
    app.post('/register', registerActionController);
    app.post('/login', loginActionController);

    app.get('/', showHomePageController);
    app.get('/register', showRegisterPageController);
    app.get('/login', showLoginPageController);
    app.get('/dashboard', showDashboardPageController);

};