const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const mustacheExpress = require('mustache-express');
const session = require('express-session');

module.exports.configApp = (app) =>{
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(express.static(path.join(__dirname, '../public')));
    app.use(session({
        secret: 'secretx231',
        resave: false,
        saveUninitialized: true,
        cookie: {
            maxAge: 1000 * 60 * 60 * 24
        }
    }));

    app.engine('mustache', mustacheExpress());

    app.set('view engine', 'mustache');
    app.set('views',path.join(__dirname, '../pages'));
};