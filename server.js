const express = require('express');
const {connectToDatabase} = require('./config/database');
const {registerRoutes} = require('./config/routes');
const {configApp} = require('./config/app');

const port = 3000;

const app = express();

configApp(app);
connectToDatabase();
registerRoutes(app);

app.listen(port, () => console.log(`server listening on port ${port}!`));


